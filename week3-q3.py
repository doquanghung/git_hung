# Question 3

import numpy as np
import random as rd
d1 = input ("Get a Depth number from user:")
h1 = input ("Get a Height number from user:")
w1 = input ("Get a Width number from user:")

d = int(d1)
h= int(h1)
w = int(w1)

master = [[0]*1 for i in range(d)]

for i in range(0,d):
    master[i] = [[random.uniform(-100,100) for k in range(w)] for j in range(h)]

final = np.array(master)

print("Q3 Output:")
print(final)

#Min,Max,Sum across depths
print ('min value across depths:')
print (np.min(final,axis=0))
print ('max value across depths:')
print (np.max(final,axis=0))
print ('sum value across depths:')
print (np.sum(final,axis=0))
print("\n")

#Min,Max,Sum across height
print ('min value across Height:')
print (np.min(final,axis=1))
print ('max value across Height:')
print (np.max(final,axis=1))
print ('sum value across Height:')
print (np.sum(final,axis=1))
print("\n")


#Min,Max,Sum across width
print ('min value across Width:')
print (np.min(final,axis=2))
print ('max value across Width:')
print (np.max(final,axis=2))
print ('sum value across Width:')
print (np.sum(final,axis=2))
print("\n")