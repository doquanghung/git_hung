# Week 3 - Question 2:
import numpy as np
import random as rd
n1 = input ("Get a number N from user:")
n = int(n1)

master = [[0] for i in range(n)]

for i in range(0,n):
    master[i] = [[rd.uniform(0,10) for k in range(n)] for j in range(n)]

final = np.array(master)

print("Q2 Output:")
print(final)

#Min,Max,Sum across depths
print ('min value across depths:')
print (np.min(final,axis=0))
print ('max value across depths:')
print (np.max(final,axis=0))
print ('sum value across depths:')
print (np.sum(final,axis=0))
print("\n")

#Min,Max,Sum across height
print ('min value across Height:')
print (np.min(final,axis=1))
print ('max value across Height:')
print (np.max(final,axis=1))
print ('sum value across Height:')
print (np.sum(final,axis=1))
print("\n")

#Min,Max,Sum across width
print ('min value across Width:')
print (np.min(final,axis=2))
print ('max value across Width:')
print (np.max(final,axis=2))
print ('sum value across Width:')
print (np.sum(final,axis=2))

print("\n")
