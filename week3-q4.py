# Week 3 - Question 4

import numpy as np
import random as rd

height = 10
width = 8
t = input ("Insert a target number:")
target=int(t)


master = [[0] for i in range(height)]
for j in range(height):
    master[j] = [rd.uniform(-10,10) for i in range(width)]

result = [[0] for i in range(target)]

#Convert to array
final = np.array(master)

print("Q4 Output:")
print(final)
x = (np.absolute((final-target).flatten()))
y = (np.sort(x))[0:target]

print("\n")
print("Index of numbers closest to target number:")
for i in range(target):
    print(np.argwhere(x == y[i]))
    result[i] = float(final.flatten()[np.argwhere(x == y[i])])

print("\n")
print("Numbers closest to target number:")
print(result)